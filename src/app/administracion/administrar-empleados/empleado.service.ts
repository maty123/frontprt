import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of,Observable, throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Empleado } from './empleado';
import Swal from 'sweetalert2' ;
import { AuthService } from 'src/app/usuarios/auth.service';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private urlEndPoint:string='http://localhost:8080/';    //url a la cual el backend envia los datos, 
  private httpHeaders=new HttpHeaders({'Content-Type':'application/json'})

  constructor(
    private http: HttpClient,
    private router:Router,
    private authService:AuthService
  ) { }

  private agregarAuthorizationHeader(){
    let token=this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization', 'Bearer '+token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e):boolean{

    if(e.status==401){
      if(this.authService.isAuthenticated()){
        this.authService.logout();
      }
      this.router.navigate(['/personal']);
      return true;
    }

    if(e.status==403){
      Swal.fire('Acceso Denegado',`Hola ${this.authService.usuario.username} no tienes acceso a este recurso`,'warning');
      this.router.navigate(['/personal-inicio']);
      return true;
    }

    return false;
  }

  /**
   * getEmpleados solicita al servidor todos los empleados registrados en la BDD
   */
  getEmpleados():Observable<Empleado[]>{
    //obtiene los clientes que son entregados desde el BackEnd
    return this.http.get(this.urlEndPoint+"empleados").pipe(
      map((response)=>response as Empleado[])
    )
  }

  /**
   * getEmpleado solicita al servidor obtener los datos de un empleado especificado por id
   * @param id corresponse al id del empleado que se quiere obtener
   */
  getEmpleado(id):Observable<Empleado>{     
    return this.http.get<Empleado>(`${this.urlEndPoint}empleados/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        this.router.navigate(['/administracion-inicio/empleados']);
        console.error(e.error.mensaje);
        Swal.fire('Error al editar', e.error.mensaje,'error');
        return throwError(e);
      })  
    );
  }

  /**
   * create envia una peticion al servidor para crear un empleado
   * la respuesta del servidor es mapeada a un objeto de tipo empleado
   * @param empleado contiene los datos del empleado que se quiere crear
   */
  create(empleado:Empleado):Observable<Empleado>{ 
    return this.http.post(this.urlEndPoint+"saveEmpleado",empleado,{headers:this.agregarAuthorizationHeader()}).pipe(
      map((response:any) => response.empleado as Empleado),
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al crear el empleado', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * update envia una solicitud al servidor para actualizar un empleado
   * @param empleado contiene los datos del empleado que se quiere actualizar
   * @param id contiene el id del empleado que se desea actualizar
   */
  update(empleado:Empleado, id:number):Observable<any>{
    return this.http.put<any>(`${this.urlEndPoint}updateEmpleado/${id}`,empleado,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al editar el empleado', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * delete envia la peticion para eliminar un empleado al servidor 
   * @param id contiene el id del empleado que se desea borrar
   */
  delete(id:number):Observable<Empleado>{
    return this.http.delete<Empleado>(`${this.urlEndPoint}deleteEmpleado/${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }
        
        console.error(e.error.mensaje);
        Swal.fire('Error al eliminar el empleado', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

}
