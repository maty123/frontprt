import { Component, OnInit } from '@angular/core';
import { Revision } from 'src/app/empleado/empleado-content/revisiones/revision';
import { RevisionService } from 'src/app/empleado/empleado-content/revisiones/revision.service';
import { ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-historial-revisiones',
  templateUrl: './historial-revisiones.component.html',
  styleUrls: ['./historial-revisiones.component.css']
})
export class HistorialRevisionesComponent implements OnInit {

  revisiones:Revision[];
  revisionesFiltered:Revision[];
  _listFilter:string;
  public activarModal:string='';

 

  constructor(
    private revisionService:RevisionService,
  
  ) { }

  ngOnInit() {
    this.getRevisiones();
  
  }

  /**
   * getRevisiones solicita todas las revisiones al revision.service.ts
   * y las guarda en una variable local (this.revisiones)
   */
  getRevisiones(){
    this.revisionService.getRevisiones().subscribe(
      revisiones =>{
        this.revisiones=revisiones;
      },
      err=>{
        console.log(err);
      }
    )
    
  }

  

  get listFilter(): string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter=value;
    this.revisionesFiltered=this.listFilter ? this.performFilter(this.listFilter) : this.revisiones;
    console.log(this.revisionesFiltered);
  }

  performFilter(filterBy:string): Revision[]{
    filterBy=filterBy.toLocaleLowerCase();
    return this.revisiones.filter((revision: Revision)=>
      revision.estado.toLocaleLowerCase().indexOf(filterBy) !== -1);   
  }

  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 


}
