import { Component, OnInit } from '@angular/core';
import { RevisionService } from 'src/app/empleado/empleado-content/revisiones/revision.service';
import * as Highcharts from 'highcharts';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-administracion-content',
  templateUrl: './administracion-content.component.html',
  styleUrls: ['./administracion-content.component.css']
})
export class AdministracionContentComponent implements OnInit {

  public revisiones:any[]=[];
  public contRevisiones:number[];
  public fecha:Date;
  public contAprobadas:number=0;
  public contRechazadas:number=0;
  public options:any={};

  constructor(
    private revisionService:RevisionService
  ) { 
    this.contRevisiones=[0,0] ;
  }

  ngOnInit() {
    this.getRevisiones();
  }

  getRevisiones(){
    this.revisionService.getRevisiones().subscribe(
      response=>{
        this.revisiones=response;
        for (let i = 0; i < this.revisiones.length; i++) {
          this.fecha=new Date(this.revisiones[i].fecha);
    
          if(this.fecha.getFullYear()==2019){
            this.contRevisiones[1]++;
            if(this.revisiones[i].estado=="APROBADO"){
              this.contAprobadas++;
            }
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contRechazadas++;
            }
          }
          if(this.fecha.getFullYear()==2018){
            this.contRevisiones[0]++;
          }
        }
        this.options={
          chart: {
              type: 'column'
          },
          title: {
              text: 'Cantidad de vehiculos atendidos 2018 VS 2019'
          },
          subtitle: {
              text: 'Fuente: plantameteorracer.com'
          },
          xAxis: {
              categories: [
                  '2018',
                  '2019'
              ],
              crosshair: true
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Cantidad '
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.0f} Cantidad</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          series: [{
              name: '2018 VS 2019',
              data: this.contRevisiones
          }]
        };
    
        Highcharts.chart('container', this.options);

      },
      error=>{
        console.log(error)
      }
    )
    
    
  }
  

  



}
