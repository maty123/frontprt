import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Vehiculo } from '../vehiculo';
import {Router,ActivatedRoute} from '@angular/router'
import Swal from 'sweetalert2' ;
import { VehiculoService } from '../vehiculo.service';

@Component({
  selector: 'app-vehiculo-form',
  templateUrl: './vehiculo-form.component.html',
  styleUrls: ['./vehiculo-form.component.css']
})
export class VehiculoFormComponent implements OnInit {

  titulo:string="Agregar vehículo";
  formVehiculo:FormGroup;
  vehiculo:Vehiculo=new Vehiculo();

  constructor(private formBuilder: FormBuilder,
              private vehiculoService:VehiculoService,
              private router:Router,
              private activatedRoute:ActivatedRoute,
    ) { 
    this.formVehiculo= this.formBuilder.group({
      patente:['',[Validators.required,]],
      tipo_vehiculo:['',[Validators.required]],
      marca:['',[Validators.required]],
      modelo:['',[Validators.required]],
      nro_motor:['',[Validators.required,Validators.min(0)]],
      nro_chasis:['',[Validators.required,Validators.min(0)]],
      anio:['',[Validators.required,Validators.min(0)]],
      color:['',[Validators.required]],
      propietario:['',[Validators.required]],
      rut_propietario:['',[Validators.required]]
    })
  }

  ngOnInit() {
    this.cargarVehiculo();
  }

  /**
   * cargarVehiculo revisa si existe una variable en la url y carga los datos del vehiculo
   * segun el id que encuentre en la url, esto sirve para cuando se desea actualizar un vehiculo
   * carga automaticamente sus datos en el formulario
   */
  public cargarVehiculo():void{  
    this.activatedRoute.params.subscribe(params=>{ 
      let id = params['id'];
      if(id){
        this.vehiculoService.getVehiculo(id).subscribe((vehiculo)=>this.vehiculo=vehiculo) 
        console.log(this.vehiculo);
      }
    })
  }

  /**
   * create solicita al vehiculo.service.ts agregar un nuevo vehiculo
   */
  public create():void{
    console.log(this.formVehiculo.value);
    this.vehiculoService.create(this.formVehiculo.value)
      .subscribe(
        vehiculo => {     
          //si se ha creado de manera exitosa se redirije a la vista principal de vehiculos
          this.router.navigate(['/personal-inicio/vehiculos']); 
          Swal.fire('Nuevo vehiculo', `vehiculo con patente:${vehiculo.patente} creada con Exito`, 'success');    
        },
        err=>{
          console.log(err);
        }
    );
  }
  
  /**
   * update solicita al vehiculo.service.ts actualizar un vehiculo
   */
  public update():void{
    this.vehiculoService.update(this.formVehiculo.value,this.vehiculo.id)
    .subscribe(
      json=>{   
        this.router.navigate(['/personal-inicio/vehiculos']);
        Swal.fire('Revision Actualizada', `vehiculo con patente ${json.patente} Actualizado con Exito`, 'success');  
      },
      err=>{
        console.log(err);
      });
  }

  /**
   * saveData asigna los valores del formulario al objeto this.vehiculo,
   * y reinicia el formulario una vez se envian los datos
   */
  saveData(){
    this.vehiculo.patente=this.formVehiculo.value.patente;
    this.vehiculo.tipo_vehiculo=this.formVehiculo.value.tipo_vehiculo;
    this.vehiculo.modelo=this.formVehiculo.value.modelo;
    this.vehiculo.marca=this.formVehiculo.value.marca;
    this.vehiculo.anio=this.formVehiculo.value.anio;
    this.vehiculo.color=this.formVehiculo.value.color;
    this.vehiculo.nro_chasis=this.formVehiculo.value.nro_chasis;
    this.vehiculo.nro_motor=this.formVehiculo.value.nro_motor;
    this.vehiculo.propietario=this.formVehiculo.value.propietario;
    this.vehiculo.rut_propietario=this.formVehiculo.value.rut_propietario;
    this.formVehiculo.reset();
  }

}
