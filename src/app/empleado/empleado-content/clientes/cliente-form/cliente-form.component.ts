import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import { ClienteService } from '../cliente.service';
import { Cliente } from '../cliente';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2' ;

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {

  cliente:Cliente=new Cliente();
  titulo:string="Agregar cliente";
  formCliente:FormGroup;

  constructor(private clienteService:ClienteService, 
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder: FormBuilder) {
    
      this.formCliente= this.formBuilder.group({
        rut:['',[Validators.required,]],
        nombre:['',[Validators.required]],
        correo:['',[Validators.required]],
        telefono:['',[Validators.required]],
      })
  }

  ngOnInit() {
    this.cargarCliente();
  }

  /**
   * cargarCliente revisa si existe una variable en la url, si hay una variable
   * solicita al cliente.service.ts que le envie el cliente especificado por id
   * y carga los datos del cliente al formulario
   */
  public cargarCliente():void{   
    this.activatedRoute.params.subscribe(params=>{  
      let id = params['id'];
      if(id){
        this.clienteService.getCliente(id).subscribe((cliente)=>this.cliente=cliente);
      }
    })
  }

  /**
   * create solicita al cliente.service.ts agregar un nuevo cliente 
   */
  public create():void{
    this.clienteService.create(this.formCliente.value)
      .subscribe(
      cliente => {   
        //en caso de exito redirecciona a la ruta principal de clientes  
        this.router.navigate(['/personal-inicio/clientes']);  
        Swal.fire('Nuevo Cliente', `Cliente ${cliente.nombre} creado con Exito`, 'success');    
      },
      err=>{
        console.log(err)
      }
    );
  }
  
  /**
   * update solicita al cliente.service.ts actualizar un cliente
   */
  public update():void{
    this.clienteService.update(this.formCliente.value,this.cliente.id)
    .subscribe(
      json=>{  
        //en caso de exito redirecciona a la vista principal de clientes
        this.router.navigate(['/personal-inicio/clientes'])
        Swal.fire('Cliente Actualizado', `Cliente ${json.cliente.nombre} Actualizado con Exito`, 'success')  
      },
      err=>{
        console.log(err);
      });
  }

  /**
   * saveData asigna los valores del formulario a la variable this.cliente y
   * luego reinicia los campos del formulario al enviar los datos
   */
  saveData(){
    this.cliente.nombre=this.formCliente.value.nombre;
    this.cliente.correo=this.formCliente.value.correo;
    this.cliente.rut=this.formCliente.value.rut;
    this.cliente.telefono=this.formCliente.value.telefono;
    
    this.formCliente.reset();
  }

}
