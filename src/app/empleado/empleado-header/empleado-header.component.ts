import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/usuarios/auth.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2' ;



@Component({
  selector: 'app-empleado-header',
  templateUrl: './empleado-header.component.html',
  styleUrls: ['./empleado-header.component.css']
})
export class EmpleadoHeaderComponent implements OnInit {

  constructor(
    public authService:AuthService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  logout():void{
    let username=this.authService.usuario.username;
    this.authService.logout();
    swal.fire('Logout',`Hola ${username} has cerrado sesión con éxito`,'success');
    this.router.navigate(['/personal']);
  }

}
