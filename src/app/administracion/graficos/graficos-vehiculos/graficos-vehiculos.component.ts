import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { RevisionService } from 'src/app/empleado/empleado-content/revisiones/revision.service';
import { VehiculoService } from 'src/app/empleado/empleado-content/vehiculos/vehiculo.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);


@Component({
  selector: 'app-graficos-vehiculos',
  templateUrl: './graficos-vehiculos.component.html',
  styleUrls: ['./graficos-vehiculos.component.css']
})
export class GraficosVehiculosComponent implements OnInit {

  public options: any = {}

  public revisiones:any[]=[];
  public vehiculos:any[]=[];
  public contAños:number[];
  public contAñosAutomovil:number[];
  public contAñosCityCar:number[];
  public contAñosCamioneta:number[];
  

  constructor(
    private vehiculosService:VehiculoService,
    private revisionService:RevisionService
  ) { 
    this.contAñosCityCar=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosAutomovil=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosCamioneta=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
  }

  ngOnInit() {
    this.getVehiculos();
    this.getRevisiones();
  }

  getRevisiones(){
    this.revisionService.getRevisiones().subscribe(
      response=>{
        this.revisiones=response;
      },
      error=>{
        console.log(error)
      }
    )
  }

  getVehiculos(){
    this.vehiculosService.getVehiculos().subscribe(
      response=>{
        this.vehiculos=response;
      },
      error=>{
        console.log(error);
      }
    )
  }

  revisionesPorAnio(){
    
    this.contAños=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    
    
    console.log(this.revisiones);
    for (let i = 0; i < this.revisiones.length; i++) {
      
      switch (this.revisiones[i].vehiculo.anio){
        case 2019: {
          this.contAños[12]=this.contAños[12]+1;
          console.log(this.contAños);
          break;
        }
        case 2018:{
          this.contAños[11]=this.contAños[11]+1;
          break;
        }
        case 2017: {
          this.contAños[10]++;
          break;
        }
        case 2016:{
          this.contAños[9]++;
          break;
        }
        case 2015: {
          this.contAños[8]++;
          break;
        }
        case 2014:{
          this.contAños[7]++;
          break;
        }
        case 2013:{
          this.contAños[6]++;
          break;
        }
        case 2012: {
          this.contAños[5]++;
          break;
        }
        case 2011:{
          this.contAños[4]++;
          break;
        }
        case 2010:{
          this.contAños[3]++;
          break;
        }
        case 2009: {
          this.contAños[2]++;
          break;
        }
        case 2008:{
          this.contAños[1]++;
          break;
        }
        case 2007:{
          this.contAños[0]++;
          break;
        }
        default:{
          break;
        }

      } 
    }

    console.log(this.contAños);

    //ahora se carga el grafico
    this.options={
      chart: {
          type: 'column'
      },
      title: {
          text: 'Cantidad de vehiculos por año'
      },
      subtitle: {
          text: 'Fuente: plantameteorracer.com'
      },
      xAxis: {
          categories: [
              '2007',
              '2008',
              '2009',
              '2010',
              '2011',
              '2012',
              '2013',
              '2014',
              '2015',
              '2016',
              '2017',
              '2018',
              '2019'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Cantidad '
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Cantidad</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'vehiculos',
          data: this.contAños
      }]
    };

    Highcharts.chart('container', this.options);
  }

  tipoVehiculoPorAnio(){
    this.contAñosCityCar=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosAutomovil=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosCamioneta=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    

    for (let i = 0; i < this.revisiones.length; i++) {
      switch (this.revisiones[i].vehiculo.anio){
        case 2019: {
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[12]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[12]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[12]++;
          }
          break;
        }
        case 2018:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[11]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[11]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[11]++;
          }
          break;
        }
        case 2017: {
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[10]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[10]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[10]++;
          }
          break;
        }
        case 2016:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[9]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[9]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[9]++;
          }
          break;
        }
        case 2015: {
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[8]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[8]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[8]++;
          }
          break;
        }
        case 2014:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[7]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[7]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[7]++;
          }
          break;
        }
        case 2013:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[6]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[6]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[6]++;
          }
          break;
        }
        case 2012: {
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[5]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[5]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[5]++;
          }
          break;
        }
        case 2011:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[4]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[4]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[4]++;
          }
          break;
        }
        case 2010:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[3]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[3]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[3]++;
          }
          break;
        }
        case 2009: {
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[2]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[2]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[2]++;
          }
          break;
        }
        case 2008:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[1]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[1]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[1]++;
          }
          break;
        }
        case 2007:{
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CITY CAR"){
            this.contAñosCityCar[0]++;
          }else{
            if(this.revisiones[i].vehiculo.tipo_vehiculo=="AUTOMOVIL"){
              this.contAñosAutomovil[0]++;
            }
          }
          if(this.revisiones[i].vehiculo.tipo_vehiculo=="CAMIONETA"){
            this.contAñosCamioneta[0]++;
          }
          break;
        }
        default:{
          break;
        }
      
    }

    this.options={

      title: {
          text: 'Comparativa de revisiones Aprovadas v/s rechazadas'
      },
  
      subtitle: {
          text: 'Fuente: PRT meteor racer'
      },
  
      yAxis: {
          title: {
              text: 'Cantidad de revisiones'
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
  
      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              },
              pointStart: 2007
          }
      },
  
      series: [{
          name: 'City Car',
          data: this.contAñosCityCar
      }, {
          name: 'Automovil',
          data: this.contAñosAutomovil
      },{
          name: 'Camioneta',
          data: this.contAñosCamioneta
      }
      ],
  
      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  
    }
    Highcharts.chart('container', this.options);
    }
  }

}
