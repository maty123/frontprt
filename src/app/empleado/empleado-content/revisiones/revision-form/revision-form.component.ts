import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import { RevisionService } from '../revision.service';
import { Revision } from '../revision';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2' ;
import { VehiculoService } from '../../vehiculos/vehiculo.service';
import { EmpleadoService } from 'src/app/administracion/administrar-empleados/empleado.service';
import { Empleado } from 'src/app/administracion/administrar-empleados/empleado';
import { Vehiculo } from '../../vehiculos/vehiculo';


@Component({
  selector: 'app-revision-form',
  templateUrl: './revision-form.component.html',
  styleUrls: ['./revision-form.component.css']
})
export class RevisionFormComponent implements OnInit {

  titulo:string="Agregar revisión";
  formRevision:FormGroup;
  public empleado:Empleado;
  public vehiculo:Vehiculo;
  public empleados:any[]=[];
  public vehiculos:any[]=[];
  public newRevision:any={};
  public revision:any={};
  public estado:number=0;
  

  constructor(private revisionService:RevisionService, 
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder: FormBuilder,
    private vehiculoService: VehiculoService,
    private empleadoService:EmpleadoService) {


      this.formRevision= this.formBuilder.group({
        id_empleado:['',[Validators.required]],
        id_vehiculo:['',[Validators.required]],
        fecha:['',[Validators.required]],
        kilometraje:['',[Validators.required]],
        gases:['',[]],
        visuales:['',[]],
        alineamiento:['',[]],
        estado:['',[]],
        cont:['',[Validators.required]]
      })
  }

  ngOnInit() {
    
    this.cargarRevision();
    this.getEmpleados();
    this.getVehiculos();
    if(this.revision.gases=='bueno' && this.revision.alineamiento=='bueno' && this.revision.visuales=='bueno' && this.revision.cont <=3){
      this.estado=1;
    }else if(this.revision.gases!='bueno' ||  this.revision.alineamiento!='bueno' || this.revision.visuales!='bueno' && this.revision.cont <= 3){
      this.estado=2;
    }else{
      this.estado=3;
    }

  }

  /**
   * cargarRevision revisa si encuentra una variable en la url, si la encuentra
   * solicita al revision.service.ts que le entrege la revision especificada
   * por la id de la url, y luego carga los datos al formulario
   */
  public cargarRevision():void{  
    this.activatedRoute.params.subscribe(params=>{ 
      let idRevision = params['idRevision'];
      if(idRevision){
        this.revisionService.getRevision(idRevision).subscribe(
          (revision)=>{
            console.log(revision);
            this.revision=revision;
            console.log(this.revision);

          }
        ); 
      }
    })
  }

  public getEmpleados(){
    this.empleadoService.getEmpleados().subscribe(
      response=>{
        this.empleados=response;
        console.log(this.empleados);
      }
    )

  }

  public getVehiculos(){
    this.vehiculoService.getVehiculos().subscribe(
      vehiculos=>{
        this.vehiculos=vehiculos;
      },
      err=>{
        console.log(err);
      }
    )
  }

  /**
   * create solicita al revision.service.ts agregar una nueva revision
   */
  public create():void{

    console.log(this.formRevision.value);
    this.newRevision=this.formRevision.value;
    this.revision.alineamiento=this.newRevision.alineamiento;
    this.revision.cont=this.newRevision.cont;
    this.revision.estado=this.newRevision.estado;
    this.revision.fecha=this.newRevision.fecha;
    this.revision.gases=this.newRevision.gases;
    this.revision.kilometraje=this.newRevision.kilometraje;
    this.revision.visuales=this.newRevision.visuales;
    //this.revision=this.newRevision;
    console.log(this.newRevision);
    console.log(this.revision);
    
    this.empleadoService.getEmpleado(this.newRevision.id_empleado).subscribe(
      response=>{
        this.revision.empleado=response;
        /*console.log(this.empleado);*/
        this.vehiculoService.getVehiculo(this.newRevision.id_vehiculo).subscribe(
          response=>{
            this.revision.vehiculo=response;
          },
          error=>{
            console.log(error);
          }
        )
      },
      error=>{
        console.log(error);
      }
    )
    
    console.log(this.revision);
    

    console.log(this.revision);
    this.revisionService.create(this.revision)
      .subscribe(revision => {
        console.log(revision);    
        //en caso de ser exitoso redirecciona a la vista principal de revisiones
        this.router.navigate(['/personal-inicio/revisiones']); 
        Swal.fire('Nueva revision', `Revision creada con Exito`, 'success');    
      },
      err=>{
        console.log(err);
      }
    );
  }
  
  /**
   * update solicita a revision.service.ts actualizar una revision
   */
  public update():void{
    this.revisionService.update(this.newRevision)
    .subscribe(
      json=>{  
        this.router.navigate(['/personal-inicio/revisiones']);
        Swal.fire('Revision Actualizada', `Revision del empleado${json.revision.id_empleado} Actualizada con Exito`, 'success');  
      },
      err=>{
        console.log(err);
      });
  }

  /**
   * saveData asigna los valores del formulario a la variable this.revision y
   * reinicia el formulario una vez enviado los datos 
   */
  saveData(){
    /*this.newRevision.id_empleado=this.formRevision.value.id_empleado;
    this.newRevision.id_vehiculo=this.formRevision.value.id_vehiculo;
    this.newRevision.fecha=this.formRevision.value.fecha;
    this.newRevision.gases=this.formRevision.value.gases;
    this.newRevision.kilometraje=this.formRevision.value.kilometraje;
    this.newRevision.alineamiento=this.formRevision.value.alineamiento;
    this.newRevision.visuales=this.formRevision.value.visuales;
    this.newRevision.cont=this.formRevision.value.cont;
    this.newRevision.estado=this.formRevision.value.estado;

    this.formRevision.reset();*/
  }


}
