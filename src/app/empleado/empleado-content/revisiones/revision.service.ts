import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of,Observable, throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import { Revision } from './revision';
import Swal from 'sweetalert2' ;


@Injectable({
  providedIn: 'root'
})
export class RevisionService {

  private urlEndPoint:string='http://localhost:8080/api/';    //url a la cual el backend envia los datos, 
  private httpHeaders=new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient, private router:Router) { }

  private isNoAutorizado(e):boolean{
    if(e.status==401 || e.status==403){
      this.router.navigate(['/personal']);
      return true;
    }
    return false;
  }

  /**
   * getRevisiones solicita al servidor todas las revisiones ingresadas a la BDD
   */
  getRevisiones():Observable<any>{
    return this.http.get(this.urlEndPoint+"listReview",{headers:this.httpHeaders});
  }

  /**
   * getRevision solicita al servidor una revisin especificando su id
   * @param id corresponde al identificador de la revision que se desea obtener
   */
  getRevision(id):Observable<any>{  
    return this.http.get<Revision>(`${this.urlEndPoint}review/${id}`);
  }

  /**
   * create solicita al servidor agregar una nueva revision
   * @param revision corresponde a los datos de la revision que se desea agregar
   */
  create(revision:Revision):Observable<Revision>{  //el cliente es enviado desde cliente-form.component.ts

    return this.http.post(this.urlEndPoint+'saveReview',revision,{headers:this.httpHeaders}).pipe(
      map((response:any) => response.revision as Revision),
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al crear la revision', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * update solicita al servidor actualizar una revision
   * @param revision corresponde a los datos de la revision que se quiere actualizar
   */
  update(revision:Revision):Observable<any>{
    return this.http.put<any>(`${this.urlEndPoint}/${revision.id_revision}`,revision,{headers:this.httpHeaders}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }
        
        console.error(e.error.mensaje);
        Swal.fire('Error al editar la revision', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * delete solicita al servidor eliminar una revision de la BDD
   * @param id corresponde al id de la revision que se desea eliminar
   */
  delete(id:number):Observable<Revision>{
    return this.http.delete<Revision>(`${this.urlEndPoint}/${id}`,{headers:this.httpHeaders}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }
        
        console.error(e.error.mensaje);
        Swal.fire('Error al eliminar la revision', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }
}
