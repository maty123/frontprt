import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2' ;
import {Router,ActivatedRoute} from '@angular/router';
import { Cliente } from 'src/app/empleado/empleado-content/clientes/cliente';
import { ClienteService } from 'src/app/empleado/empleado-content/clientes/cliente.service';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-historial-clientes',
  templateUrl: './historial-clientes.component.html',
  styleUrls: ['./historial-clientes.component.css']
})
export class HistorialClientesComponent implements OnInit {

  _listFilter:string;
  clientesFiltered:Cliente[];
  clientes:Cliente[];

  constructor(
    private clienteService:ClienteService
  ) { }

  ngOnInit() {
    this.getClientes();
  }

  /**
   * getClientes obtiene los clientes desde cliente.service.ts, y guarda la respuesta
   * en las variables locales (this.clientes y this.clientesFiltered)
   */
  getClientes(){
    this.clienteService.getClientes().subscribe(
      clientes=>{
        this.clientes=clientes;
        this.clientesFiltered=clientes;
      },
      err=>{
        console.log(err)
      }
    );
  }

  get listFilter(): string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter=value;
    this.clientesFiltered=this.listFilter ? this.performFilter(this.listFilter) : this.clientes;
    console.log(this.clientesFiltered);
  }

  performFilter(filterBy:string): Cliente[]{
    filterBy=filterBy.toLocaleLowerCase();
    return this.clientes.filter((cliente: Cliente)=>
      cliente.rut.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
  
  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 

}
