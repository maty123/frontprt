import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { RevisionService } from 'src/app/empleado/empleado-content/revisiones/revision.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-graficos-revisiones',
  templateUrl: './graficos-revisiones.component.html',
  styleUrls: ['./graficos-revisiones.component.css']
})
export class GraficosRevisionesComponent implements OnInit {

  public fecha:Date;

  public options: any = {};
  public textoGraficos:boolean=true;

  public revisiones:any[]=[];
  public contAños:number[];
  public contAñosAprovados:number[];
  public contAñosRechazados:number[];
  
    
  constructor(
    private revisionService:RevisionService
  
  ) { 
    this.contAños=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosAprovados=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosRechazados=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    
  }

  ngOnInit(){

    this.getRevisiones();
    
  }


  getRevisiones(){
    this.revisionService.getRevisiones().subscribe(
      response=>{
        this.revisiones=response;
      },
      error=>{
        console.log(error)
      }
    )
  }

  revisionesPorAnio(){
    this.textoGraficos=false;
    this.contAños=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    
    
    console.log(this.revisiones);
    for (let i = 0; i < this.revisiones.length; i++) {
      
      switch (this.revisiones[i].vehiculo.anio){
        case 2019: {
          this.contAños[12]=this.contAños[12]+1;
          console.log(this.contAños);
          break;
        }
        case 2018:{
          this.contAños[11]=this.contAños[11]+1;
          break;
        }
        case 2017: {
          this.contAños[10]++;
          break;
        }
        case 2016:{
          this.contAños[9]++;
          break;
        }
        case 2015: {
          this.contAños[8]++;
          break;
        }
        case 2014:{
          this.contAños[7]++;
          break;
        }
        case 2013:{
          this.contAños[6]++;
          break;
        }
        case 2012: {
          this.contAños[5]++;
          break;
        }
        case 2011:{
          this.contAños[4]++;
          break;
        }
        case 2010:{
          this.contAños[3]++;
          break;
        }
        case 2009: {
          this.contAños[2]++;
          break;
        }
        case 2008:{
          this.contAños[1]++;
          break;
        }
        case 2007:{
          this.contAños[0]++;
          break;
        }
        default:{
          break;
        }

      } 
    }

    console.log(this.contAños);

    //ahora se carga el grafico
    this.options={
      chart: {
          type: 'column'
      },
      title: {
          text: 'Cantidad de vehiculos por año'
      },
      subtitle: {
          text: 'Fuente: plantameteorracer.com'
      },
      xAxis: {
          categories: [
              '2007',
              '2008',
              '2009',
              '2010',
              '2011',
              '2012',
              '2013',
              '2014',
              '2015',
              '2016',
              '2017',
              '2018',
              '2019'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Cantidad '
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Cantidad</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'vehiculos',
          data: this.contAños
      }]
    };

    Highcharts.chart('container', this.options);
  }

  revisionesPorAnioComparativa(){
    this.textoGraficos=false;

    this.contAñosAprovados=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;
    this.contAñosRechazados=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;

    for (let i = 0; i < this.revisiones.length; i++) {
      switch (this.revisiones[i].vehiculo.anio){
        case 2019: {
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[12]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[12]++;
            }
          }
          break;
        }
        case 2018:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[11]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[11]++;
            }
          }
          break;
        }
        case 2017: {
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[10]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[10]++;
            }
          }
          break;
        }
        case 2016:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[9]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[9]++;
            }
          }
          break;
        }
        case 2015: {
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[8]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[8]++;
            }
          }
          break;
        }
        case 2014:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[7]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[7]++;
            }
          }
          break;
        }
        case 2013:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[6]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[6]++;
            }
          }
          break;
        }
        case 2012: {
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[5]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[5]++;
            }
          }
          break;
        }
        case 2011:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[4]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[4]++;
            }
          }
          break;
        }
        case 2010:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[3]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[3]++;
            }
          }
          break;
        }
        case 2009: {
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[2]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[2]++;
            }
          }
          break;
        }
        case 2008:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[1]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[1]++;
            }
          }
          break;
        }
        case 2007:{
          if(this.revisiones[i].estado=="APROBADO"){
            this.contAñosAprovados[0]++;
          }else{
            if(this.revisiones[i].estado=="RECHAZADO"){
              this.contAñosRechazados[0]++;
            }
          }
          break;
        }
        default:{
          break;
        }
      
    }

    this.options={

      title: {
          text: 'Comparativa de revisiones Aprovadas v/s rechazadas'
      },
  
      subtitle: {
          text: 'Fuente: PRT meteor racer'
      },
  
      yAxis: {
          title: {
              text: 'Cantidad de revisiones'
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
  
      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              },
              pointStart: 2007
          }
      },
  
      series: [{
          name: 'Aprovadas',
          data: this.contAñosAprovados
      }, {
          name: 'Rechazadas',
          data: this.contAñosRechazados
      }],
  
      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  
    }
    Highcharts.chart('container', this.options);
    }
  }

  cantRevisionesPorAnio(){
    this.contAños=[0,0,0,0,0,0,0,0,0,0,0,0,0] ;

    this.fecha=new Date();
    
    
    console.log(this.revisiones);
    for (let i = 0; i < this.revisiones.length; i++) {
      this.fecha=new Date(this.revisiones[i].fecha);

      //this.fecha=this.revisiones[i].fecha;
      console.log(this.fecha);
      console.log(this.fecha.getFullYear());
      
      switch (this.fecha.getFullYear()){
        case 2019: {
          this.contAños[12]=this.contAños[12]+1;
          console.log(this.contAños);
          break;
        }
        case 2018:{
          this.contAños[11]=this.contAños[11]+1;
          break;
        }
        case 2017: {
          this.contAños[10]++;
          break;
        }
        case 2016:{
          this.contAños[9]++;
          break;
        }
        case 2015: {
          this.contAños[8]++;
          break;
        }
        case 2014:{
          this.contAños[7]++;
          break;
        }
        case 2013:{
          this.contAños[6]++;
          break;
        }
        case 2012: {
          this.contAños[5]++;
          break;
        }
        case 2011:{
          this.contAños[4]++;
          break;
        }
        case 2010:{
          this.contAños[3]++;
          break;
        }
        case 2009: {
          this.contAños[2]++;
          break;
        }
        case 2008:{
          this.contAños[1]++;
          break;
        }
        case 2007:{
          this.contAños[0]++;
          break;
        }
        default:{
          break;
        }

      } 
    }

    console.log(this.contAños);

    //ahora se carga el grafico
    this.options={
      chart: {
          type: 'column'
      },
      title: {
          text: 'Cantidad de vehiculos por año'
      },
      subtitle: {
          text: 'Fuente: plantameteorracer.com'
      },
      xAxis: {
          categories: [
              '2007',
              '2008',
              '2009',
              '2010',
              '2011',
              '2012',
              '2013',
              '2014',
              '2015',
              '2016',
              '2017',
              '2018',
              '2019'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Cantidad '
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} Cantidad</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'vehiculos',
          data: this.contAños
      }]
    };

    Highcharts.chart('container', this.options);
  }




}
