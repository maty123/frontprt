import { Component, OnInit } from '@angular/core';
import { Revision } from './revision';
import { RevisionService } from './revision.service';
import Swal from 'sweetalert2' ;

@Component({
  selector: 'app-revisiones',
  templateUrl: './revisiones.component.html',
  styleUrls: ['./revisiones.component.css']
})
export class RevisionesComponent implements OnInit {

  revisiones:any[]=[];
  _listFilter:string;
  revisionesFiltered:any[]=[];

  constructor(private revisionService:RevisionService) { }

  ngOnInit() {
    this.getRevisiones()
  }

  /**
   * getRevisiones solicita todas las revisiones al revision.service.ts
   * y las guarda en una variable local (this.revisiones)
   */
  getRevisiones(){
    this.revisionService.getRevisiones().subscribe(
      revisiones =>{
        this.revisiones=revisiones;
        this.revisionesFiltered=this.revisiones;
      },
      err=>{
        console.log(err);
      }
    )
  }
  
  /**
   * delete solicita eliminar una revision al revision.service.ts
   * pero antes muestra una ventana de confirmación
   */
  delete(revision:Revision):void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Está seguro',
      text: `¿Está seguro que desea eliminar la revision numero: ${revision.id_revision} del vehiculo: ${revision.vehiculo.id}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.revisionService.delete(revision.id_revision).subscribe(
          response=>{
            this.revisiones=this.revisiones.filter(rev=>rev!==revision)
            swalWithBootstrapButtons.fire(
              'Revision Eliminada!',
              `Revision ${revision.id_revision}, del vehículo ${revision.vehiculo.id}  eliminado con éxito.`,
              'success'
            )
          }
        )
        
      }
    })
  }

  get listFilter(): string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter=value;
    this.revisionesFiltered=this.listFilter ? this.performFilter(this.listFilter) : this.revisiones;
    console.log(this.revisionesFiltered);
  }

  performFilter(filterBy:string):any[] {
    filterBy=filterBy.toLocaleLowerCase();
    return this.revisiones.filter((revision: any)=>
      revision.vehiculo.patente.toLocaleLowerCase().indexOf(filterBy) !== -1);   
  }

}
