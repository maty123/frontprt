import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Vehiculo } from './vehiculo';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2' ;

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  private urlEndPoint:string='http://localhost:8080/';    
  private urlEndPoint2:string="http://localhost:8080/update/";
  private urlEndPoint3:string='http://localhost:8080/deleteVehicle/';
  private httpHeaders=new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient, private router:Router) { }

  private isNoAutorizado(e):boolean{
    if(e.status==401 || e.status==403){
      this.router.navigate(['/personal']);
      return true;
    }
    return false;
  }

  /**
   * getVehiculos solicita al servidor todos los vehiculos registrados en la BDD
   */
  getVehiculos():Observable<Vehiculo[]>{
    return this.http.get(this.urlEndPoint+"vehicle").pipe(
      map((response)=>response as Vehiculo[])
    )
  }
  
  /**
   * getVehiculo devuelve un vehiculo especificando su id
   * @param id corresponde  a la id del vehiculo que se desea obtener
   */
  getVehiculo(id):Observable<Vehiculo>{      
    return this.http.get<Vehiculo>(`${this.urlEndPoint}vehicle/${id}`).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        this.router.navigate(['/personal-inicio/vehiculos']); 
        console.error(e.error.mensaje);
        Swal.fire('Error al editar', e.error.mensaje,'error');
        return throwError(e);
      })  
    );
  }

  /**
   * create solicita al servidor registrar un nuevo vehiculo
   * @param vehiculo corresponden a los datos del vehiculo que se desea registrar
   */
  create(vehiculo:Vehiculo):Observable<Vehiculo>{  //el cliente es enviado desde cliente-form.component.ts
    return this.http.post(this.urlEndPoint+"saveVehicle",vehiculo,{headers:this.httpHeaders}).pipe(
      map((response:any) => response.vehiculo as Vehiculo),
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al crear el vehiculo', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * update solicita al servidor actualizar un vehiculo
   * @param vehiculo corrresponden a los datos del vehiculo que se quiere actualizar
   * @param id es el identificador del vehiculo que se quiere actualizar
   */
  update(vehiculo:Vehiculo,id:number):Observable<any>{
    return this.http.put<any>(`${this.urlEndPoint2}${id}`,vehiculo,{headers:this.httpHeaders}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al editar el vehiculo', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * delete solicita al servidor eliminar un vehiculo de la BDD
   * @param id corresponde al identificador del vehiculo que se desea eliminar
   */
  delete(id):Observable<Vehiculo>{
    return this.http.delete<Vehiculo>(`${this.urlEndPoint3}${id}`,{headers:this.httpHeaders}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al eliminar el vehiculo', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }
}
