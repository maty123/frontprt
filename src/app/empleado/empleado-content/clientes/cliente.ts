export class Cliente{
    id:number;
    rut:string;
    nombre:string;
    telefono:number;
    correo:string;
}