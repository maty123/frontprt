import { Component, OnInit } from '@angular/core';
import { RevisionService } from '../revisiones/revision.service';
import {Router,ActivatedRoute} from '@angular/router'
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-detalle-revision',
  templateUrl: './detalle-revision.component.html',
  styleUrls: ['./detalle-revision.component.css']
})
export class DetalleRevisionComponent implements OnInit {

  detalleRevision:any={};
  public fecha_expiracion:Date;


  constructor(
    private _revisionService:RevisionService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
  ) { }

  ngOnInit() {
    this.obtenerDetalle();
  }


  obtenerDetalle(){
    this.activatedRoute.params.subscribe(params=>{ 
      let id = params['id'];
      if(id){
        this._revisionService.getRevision(id).subscribe(
          response=>{
            this.detalleRevision=response;
            this.fecha_expiracion=new Date(this.detalleRevision.fecha);
            this.fecha_expiracion.setFullYear(this.fecha_expiracion.getFullYear()+1);
            console.log(this.fecha_expiracion);

          }
        )
      }
    });
 
  }

  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 

  public generatePermisoPDF(){

    var data = document.getElementById('contentToConvertPermiso'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 

  }
}
