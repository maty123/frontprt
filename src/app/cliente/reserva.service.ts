import { Injectable } from '@angular/core';
import { Reserva } from './reserva';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  private urlEndPoint:string='http://localhost:8080/';  
  private httpHeaders=new HttpHeaders({'Content-Type':'application/json'})

  constructor(
    private http:HttpClient,
    private router:Router
  ) { }

  /**
   * getReservas solicita a la BDD todas las reservas registradas
   */
  getReservas():Observable<Reserva[]>{
    return this.http.get(this.urlEndPoint+"reservas").pipe(
      map((response)=>response as Reserva[])
    )
  }

  create(reserva:Reserva):Observable<Reserva>{  
    return this.http.post(this.urlEndPoint+"saveReserva",reserva,{headers:this.httpHeaders}).pipe(
      map((response:any) => response.reserva as Reserva),
      catchError(e=>{

      

        console.error(e.error.mensaje);
        Swal.fire('Error al crear el vehiculo', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }


}
