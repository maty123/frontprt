import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficosClientesComponent } from './graficos-clientes.component';

describe('GraficosClientesComponent', () => {
  let component: GraficosClientesComponent;
  let fixture: ComponentFixture<GraficosClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficosClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficosClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
