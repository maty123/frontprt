import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router'  //necesario para implementar rutas 
import {HttpClientModule} from '@angular/common/http'; 
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {AuthGuard} from './usuarios/guards/auth.guard';

import { AppComponent } from './app.component';
import { ClienteComponent } from './cliente/cliente.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { ClienteHeaderComponent } from './cliente/cliente-header/cliente-header.component';
import { ClienteContentComponent } from './cliente/cliente-content/cliente-content.component';
import { ClienteFooterComponent } from './cliente/cliente-footer/cliente-footer.component';
import { EmpleadoHeaderComponent } from './empleado/empleado-header/empleado-header.component';
import { EmpleadoContentComponent } from './empleado/empleado-content/empleado-content.component';
import { EmpleadoFooterComponent } from './empleado/empleado-footer/empleado-footer.component';
import { ReservaHoraComponent } from './cliente/cliente-content/reserva-hora/reserva-hora.component';
import { ConsideracionesComponent } from './cliente/cliente-content/consideraciones/consideraciones.component';
import { EmpleadoLoginComponent } from './empleado/empleado-login/empleado-login.component';
import { RevisionesComponent } from './empleado/empleado-content/revisiones/revisiones.component';
import { VehiculosComponent } from './empleado/empleado-content/vehiculos/vehiculos.component';
import { ClientesComponent } from './empleado/empleado-content/clientes/clientes.component';
import { ClienteFormComponent } from './empleado/empleado-content/clientes/cliente-form/cliente-form.component';
import { RevisionFormComponent } from './empleado/empleado-content/revisiones/revision-form/revision-form.component';
import { VehiculoFormComponent } from './empleado/empleado-content/vehiculos/vehiculo-form/vehiculo-form.component';
import { ClienteService } from './empleado/empleado-content/clientes/cliente.service';
import { AdministracionComponent } from './administracion/administracion.component';
import { AdministracionContentComponent } from './administracion/administracion-content/administracion-content.component';
import { AdministracionMenuComponent } from './administracion/administracion-menu/administracion-menu.component';
import { AdministracionLoginComponent } from './administracion/administracion-login/administracion-login.component';
import { PreciosComponent } from './cliente/cliente-content/precios/precios.component';
import { TiemposComponent } from './cliente/cliente-content/tiempos/tiempos.component';
import { HistorialRevisionesComponent } from './administracion/administracion-content/historial-revisiones/historial-revisiones.component';
import { HistorialVehiculosComponent } from './administracion/administracion-content/historial-vehiculos/historial-vehiculos.component';
import { HistorialClientesComponent } from './administracion/administracion-content/historial-clientes/historial-clientes.component';
import { GraficosRevisionesComponent } from './administracion/graficos/graficos-revisiones/graficos-revisiones.component';
import { GraficosVehiculosComponent } from './administracion/graficos/graficos-vehiculos/graficos-vehiculos.component';
import { GraficosClientesComponent } from './administracion/graficos/graficos-clientes/graficos-clientes.component';
import { AdministrarEmpleadosComponent } from './administracion/administrar-empleados/administrar-empleados.component';
import { EmpleadoFormComponent } from './administracion/administrar-empleados/empleado-form/empleado-form.component';
import { DetalleRevisionComponent } from './empleado/empleado-content/detalle-revision/detalle-revision.component';
import { InformeRevisionComponent } from './administracion/administracion-content/informe-revision/informe-revision.component';



const routes:Routes=[
  {path:'',redirectTo:'/inicio',pathMatch:'full'},   //como pagina de inicio se debe dejar la vista para los clientes
  {path:'inicio',component:ClienteComponent,
    children: [
      {
        path:'',
        component:ClienteContentComponent
      },
      {
        path:'reserva',
        component:ReservaHoraComponent
      },
      {
        path:'consideraciones',
        component:ConsideracionesComponent
      },
      {
        path:'precios',
        component:PreciosComponent
      },
      {
        path:'tiempos',
        component:TiemposComponent
      }
    ]
  },
  {path:'personal',component:EmpleadoLoginComponent},
  {path:'personal-inicio',component:EmpleadoComponent,canActivate:[AuthGuard],
    children:[
      {
        path:"",
        component:EmpleadoContentComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'revisiones',
        component:RevisionesComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'vehiculos',
        component:VehiculosComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'clientes',
        component:ClientesComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'clientes-form',
        component:ClienteFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'clientes-form/:id',
        component:ClienteFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'revisiones-form',
        component:RevisionFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'revisiones-form/:idRevision',
        component:RevisionFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'vehiculos-form',
        component:VehiculoFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'vehiculos-form/:id',
        component:VehiculoFormComponent,
        canActivate:[AuthGuard]
      },
      {
        path:'detalle-revision/:id',
        component:DetalleRevisionComponent
      }
    ]
  },
  {path:'administracion',component:AdministracionLoginComponent},
  {path:'administracion-inicio',component:AdministracionComponent,
    children:[
      {
        path:"",
        component:AdministracionContentComponent
      },
      {
        path:"historial-revisiones",
        component:HistorialRevisionesComponent
      },
      {
        path:"informe-revision/:id",
        component:InformeRevisionComponent
      },
      {
        path:"historial-vehiculos",
        component:HistorialVehiculosComponent
      },
      {
        path:"historial-clientes",
        component:HistorialClientesComponent
      },
      {
        path:"graficos-revisiones",
        component:GraficosRevisionesComponent
      },
      {
        path:"graficos-vehiculos",
        component:GraficosVehiculosComponent
      },
      {
        path:"graficos-clientes",
        component:GraficosClientesComponent
      },
      {
        path:"empleados",
        component:AdministrarEmpleadosComponent
      },
      {
        path:'empleados-form',
        component:EmpleadoFormComponent
      },
      {
        path:'empleados-form/:id',
        component:EmpleadoFormComponent
      }
    ]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    EmpleadoComponent,
    ClienteHeaderComponent,
    ClienteContentComponent,
    ClienteFooterComponent,
    EmpleadoHeaderComponent,
    EmpleadoContentComponent,
    EmpleadoFooterComponent,
    ReservaHoraComponent,
    ConsideracionesComponent,
    EmpleadoLoginComponent,
    RevisionesComponent,
    VehiculosComponent,
    ClientesComponent,
    ClienteFormComponent,
    RevisionFormComponent,
    VehiculoFormComponent,
    AdministracionComponent,
    AdministracionContentComponent,
    AdministracionMenuComponent,
    AdministracionLoginComponent,
    PreciosComponent,
    TiemposComponent,
    HistorialRevisionesComponent,
    HistorialVehiculosComponent,
    HistorialClientesComponent,
    GraficosRevisionesComponent,
    GraficosVehiculosComponent,
    GraficosClientesComponent,
    AdministrarEmpleadosComponent,
    EmpleadoFormComponent,
    DetalleRevisionComponent,
    InformeRevisionComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),    //se le entrega nuestro arreglo con las rutas definidas
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
