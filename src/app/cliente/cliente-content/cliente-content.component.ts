import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cliente-content',
  templateUrl: './cliente-content.component.html',
  styleUrls: ['./cliente-content.component.css']
})
export class ClienteContentComponent implements OnInit, OnDestroy {

  subscription:Subscription;

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
    this.subscription= this.router.events.pipe(
      filter(event=> event instanceof NavigationEnd)
    )
    .subscribe(()=>window.scrollTo(0,0));
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
