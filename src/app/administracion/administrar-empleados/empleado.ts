export class Empleado{
    id_empleado:number;
    rut_empleado:string;
    nombre:string;
    sexo:string;
    direccion:string;
    correo:string;
    telefono:number;
}