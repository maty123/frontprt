import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/usuarios/auth.service';

@Component({
  selector: 'app-administracion-menu',
  templateUrl: './administracion-menu.component.html',
  styleUrls: ['./administracion-menu.component.css']
})
export class AdministracionMenuComponent implements OnInit {

  mostrarSidebar:boolean=true;

  constructor(
    public authService:AuthService
  ) { }

  ngOnInit() {
  }

  ocultarSidebar(){
    if(this.mostrarSidebar==true){
      this.mostrarSidebar=false;
    }else{
      this.mostrarSidebar=true;
    }
  }

}
