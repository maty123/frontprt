import { Component, OnInit } from '@angular/core';
import {  Empleado } from './empleado';
import { EmpleadoService } from './empleado.service';
import Swal from 'sweetalert2' ;
import {Router,ActivatedRoute} from '@angular/router';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-administrar-empleados',
  templateUrl: './administrar-empleados.component.html',
  styleUrls: ['./administrar-empleados.component.css']
})
export class AdministrarEmpleadosComponent implements OnInit {

  _listFilter:string;
  empleadosFiltered:Empleado[];
  empleados:Empleado[];

  constructor(
    private empleadoService:EmpleadoService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getEmpleados();
  }

  /**
   * getClientes obtiene los clientes desde cliente.service.ts, y guarda la respuesta
   * en las variables locales (this.clientes y this.clientesFiltered)
   */
  getEmpleados(){
    this.empleadoService.getEmpleados().subscribe(
      empleados=>{
        this.empleados=empleados;
        this.empleadosFiltered=empleados;
      },
      err=>{
        console.log(err)
      }
    );
  }

  /**
   * delete solicita eliminar un empleado al empleado.service.ts, pero antes
   * ejecuta una ventana para confirmar si esta seguro de eliminar el empleado
   * @param empleado contiene datos del empleado que se desea eliminar
   */
  delete(empleado:Empleado):void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Está seguro',
      text: `¿Está seguro que desea eliminar al empleado ${empleado.nombre} ?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.empleadoService.delete(empleado.id_empleado).subscribe(
          response=>{
            this.empleadosFiltered=this.empleados.filter(emp=>emp!==empleado)
            swalWithBootstrapButtons.fire(
              'Empleado Eliminado!',
              `Empleado ${empleado.nombre}  eliminado con éxito.`,
              'success'
            )
            
          }
        )
        
      }
    })
  }

  get listFilter(): string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter=value;
    this.empleadosFiltered=this.listFilter ? this.performFilter(this.listFilter) : this.empleados;
    console.log(this.empleadosFiltered);
  }

  performFilter(filterBy:string): Empleado[]{
    filterBy=filterBy.toLocaleLowerCase();
    return this.empleados.filter((empleado: Empleado)=>
      empleado.rut_empleado.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 
}
