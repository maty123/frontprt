import { Component, OnInit } from '@angular/core';
import { Reserva } from '../../reserva';
import { FormGroup } from '@angular/forms';
import { ReservaService } from '../../reserva.service';
import Swal from 'sweetalert2' ;

@Component({
  selector: 'app-reserva-hora',
  templateUrl: './reserva-hora.component.html',
  styleUrls: ['./reserva-hora.component.css']
})
export class ReservaHoraComponent implements OnInit {

  public reserva:Reserva=new Reserva();
  

  constructor(
    private reservaService:ReservaService
  ) {
    this.reserva.fecha=new Date();
    this.reserva.hora="";
    this.reserva.rut="";
   }

  ngOnInit() {
  }

  create(){
    this.reservaService.create(this.reserva).subscribe(
      response=>{
        console.log(response)
        Swal.fire('Reserva lista', `Reserva realizada con Exito!!`, 'success');    
      },
      error=>{
        console.log(error)
      }
    )
  }

}
