import { Component, OnInit } from '@angular/core';
import { RevisionService } from 'src/app/empleado/empleado-content/revisiones/revision.service';
import { ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-informe-revision',
  templateUrl: './informe-revision.component.html',
  styleUrls: ['./informe-revision.component.css']
})
export class InformeRevisionComponent implements OnInit {

  revision:any={};
  public fecha_expiracion:Date;

  constructor(
    private revisionService:RevisionService,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit(
  ) {
    this.obtenerDetalle();
  }

  obtenerDetalle(){
    this.activatedRoute.params.subscribe(params=>{ 
      let id = params['id'];
      if(id){
        this.revisionService.getRevision(id).subscribe(
          response=>{
            this.revision=response;
            this.fecha_expiracion=new Date(this.revision.fecha);
            this.fecha_expiracion.setFullYear(this.fecha_expiracion.getFullYear()+1);
            console.log(this.fecha_expiracion);

          }
        )
      }
    });
    
  }

  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 

 
}
