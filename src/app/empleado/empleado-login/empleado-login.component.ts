import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/usuarios/usuario';
import {AuthService} from '../../usuarios/auth.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2' ;

@Component({
  selector: 'app-empleado-login',
  templateUrl: './empleado-login.component.html',
  styleUrls: ['./empleado-login.component.css']
})
export class EmpleadoLoginComponent implements OnInit {

  usuario:Usuario;

  constructor(
    private authService:AuthService,
    private router:Router

  ) {
    this.usuario=new Usuario();
  }

  ngOnInit() {
    if(this.authService.isAuthenticated()){
      swal.fire('Login',`Hola ${this.authService.usuario.username} ya estas autenticado!`,'info');
      this.router.navigate(['/personal-inicio']);
    }
  }

  login():void{
    console.log(this.usuario);
    if(this.usuario.username==null || this.usuario.password==null){
      swal.fire('Error Login','Username o password vacías!','error');
    }

    this.authService.login(this.usuario).subscribe(
      response=>{
        console.log(response);
        let payload=JSON.parse(atob(response.access_token.split(".")[1]));
        console.log(payload);

        this.authService.guardarUsuario(response.access_token);
        this.authService.guardarToken(response.access_token);
        let usuario=this.authService.usuario;
        console.log(usuario);
        this.router.navigate(['/personal-inicio']);
        swal.fire('Login','Hola '+ usuario.username+', has iniciado sesión con éxito!','success');
      },
      err=>{
        if(err.status==400){
          swal.fire('Error Login','Usuario o clave incorrectas!','error');
        }
      }
    );
  }

}
