import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'
import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../empleado';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2' ;

@Component({
  selector: 'app-empleado-form',
  templateUrl: './empleado-form.component.html',
  styleUrls: ['./empleado-form.component.css']
})
export class EmpleadoFormComponent implements OnInit {

  empleado:Empleado=new Empleado();
  titulo:string="Agregar empleado";
  formEmpleado:FormGroup;

  constructor(
    private empleadoService:EmpleadoService, 
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder: FormBuilder
  ) { 
    this.formEmpleado= this.formBuilder.group({
      rut_empleado:['',[Validators.required,]],
      nombre:['',[Validators.required]],
      correo:['',[Validators.required]],
      telefono:['',[Validators.required]],
      direccion:['',[Validators.required]],
      sexo:['',[Validators.required]]
    })
  }

  ngOnInit() {
    this.cargarEmpleado();
  }

  /**
   * cargarEmpelado revisa si existe una variable en la url, si hay una variable
   * solicita al empleado.service.ts que le envie el empleado especificado por id
   * y carga los datos del empleado al formulario
   */
  public cargarEmpleado():void{   
    this.activatedRoute.params.subscribe(params=>{  
      let id = params['id'];
      if(id){
        this.empleadoService.getEmpleado(id).subscribe((empleado)=>this.empleado=empleado);
      }
    })
  }

    /**
   * create solicita al empleado.service.ts agregar un nuevo empleado 
   */
  public create():void{
    this.empleadoService.create(this.formEmpleado.value)
      .subscribe(
      empleado => {   
        //en caso de exito redirecciona a la ruta principal de clientes  
        this.router.navigate(['/administracion-inicio/empleados']);  
        Swal.fire('Nuevo Empleado', `Empleado ${empleado.nombre} creado con Exito`, 'success');    
      },
      err=>{
        console.log(err)
      }
    );
  }

  /**
   * update solicita al empleado.service.ts actualizar un empleado
   */
  public update():void{
    this.empleadoService.update(this.formEmpleado.value,this.empleado.id_empleado)
    .subscribe(
      json=>{  
        //en caso de exito redirecciona a la vista principal de clientes
        this.router.navigate(['/administracion-inicio/empleados'])
        Swal.fire('Empleado Actualizado', `Empleado ${json.empleado.nombre} Actualizado con Exito`, 'success')  
      },
      err=>{
        console.log(err);
      });
  }

   /**
   * saveData asigna los valores del formulario a la variable this.empleado y
   * luego reinicia los campos del formulario al enviar los datos
   */
  saveData(){
    this.empleado.nombre=this.formEmpleado.value.nombre;
    this.empleado.correo=this.formEmpleado.value.correo;
    this.empleado.rut_empleado=this.formEmpleado.value.rut_empleado;
    this.empleado.telefono=this.formEmpleado.value.telefono;
    
    this.formEmpleado.reset();
  }

}
