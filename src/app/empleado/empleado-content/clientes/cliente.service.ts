import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of,Observable, throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import { Cliente } from './cliente';
import Swal from 'sweetalert2' ;
import { AuthService } from 'src/app/usuarios/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urlEndPoint:string='http://localhost:8080/api/';    //url a la cual el backend envia los datos, 
  private urlEndPoint2:string='http://localhost:8080/api/clientes/';
  private urlEndPoint3:string='http://localhost:8080/api/DeleteClientes/';
  private httpHeaders=new HttpHeaders({'Content-Type':'application/json'})

  

  constructor(
    private http: HttpClient,
    private router:Router,
    private authService:AuthService
    ) 
  { }

  private agregarAuthorizationHeader(){
    let token=this.authService.token;
    if(token!=null){
      return this.httpHeaders.append('Authorization', 'Bearer '+token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e):boolean{

    if(e.status==401){
      if(this.authService.isAuthenticated()){
        this.authService.logout();
      }
      this.router.navigate(['/personal']);
      return true;
    }

    if(e.status==403){
      Swal.fire('Acceso Denegado',`Hola ${this.authService.usuario.username} no tienes acceso a este recurso`,'warning');
      this.router.navigate(['/personal-inicio']);
      return true;
    }

    return false;
  }

  /**
   * getClientes solicita al servidor todos los clientes registrados en la BDD
   */
  getClientes():Observable<Cliente[]>{
    //obtiene los clientes que son entregados desde el BackEnd
    return this.http.get(this.urlEndPoint+"customer").pipe(
      map((response)=>response as Cliente[])
    )
   
  }

  /**
   * getCliente solicita al servidor obtener los datos de un cliente especificado por id
   * @param id corresponse al id del cliente que se quiere obtener
   */
  getCliente(id):Observable<Cliente>{     
    return this.http.get<Cliente>(`${this.urlEndPoint2}${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        this.router.navigate(['/personal-inicio/clientes']);
        console.error(e.error.mensaje);
        Swal.fire('Error al editar', e.error.mensaje,'error');
        return throwError(e);
      })  
    );
  }

  /**
   * create envia una peticion al servidor para crear un cliente
   * la respuesta del servidor es mapeada a un objeto de tipo cliente
   * @param cliente contiene los datos del cliente que se quiere crear
   */
  create(cliente:Cliente):Observable<Cliente>{  //el cliente es enviado desde cliente-form.component.ts
    return this.http.post(this.urlEndPoint+"save",cliente,{headers:this.agregarAuthorizationHeader()}).pipe(
      map((response:any) => response.cliente as Cliente),
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al crear el cliente', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * update envia una solicitud al servidor para actualizar un cliente
   * @param cliente contiene los datos del cliente que se quiere actualizar
   * @param id contiene el id del cliente que se desea actualizar
   */
  update(cliente:Cliente, id:number):Observable<any>{
    return this.http.put<any>(`${this.urlEndPoint}update/${id}`,cliente,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al editar el cliente', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  /**
   * delete envia la peticion para eliminar un cliente al servidor 
   * @param id contiene el id del cliente que se desea borrar
   */
  delete(id:number):Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndPoint3}${id}`,{headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e=>{

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }
        
        console.error(e.error.mensaje);
        Swal.fire('Error al eliminar el cliente', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

}
