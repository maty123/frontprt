import { Vehiculo } from '../vehiculos/vehiculo';
import { Empleado } from 'src/app/administracion/administrar-empleados/empleado';

export class Revision{
    id_revision:number;
    cont:number;
    estado:string;
    fecha:Date;
    kilometraje:number;
    gases:string;
    visuales:string;
    alineamiento:string;
    vehiculo:Vehiculo;
    empleado:Empleado;
}