import { Component, OnInit } from '@angular/core';
import { Vehiculo } from 'src/app/empleado/empleado-content/vehiculos/vehiculo';
import { VehiculoService } from 'src/app/empleado/empleado-content/vehiculos/vehiculo.service';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-historial-vehiculos',
  templateUrl: './historial-vehiculos.component.html',
  styleUrls: ['./historial-vehiculos.component.css']
})
export class HistorialVehiculosComponent implements OnInit {

  _listFilter:string;
  vehiculosFiltered:Vehiculo[];
  vehiculos:Vehiculo[];

  constructor(
    private vehiculoService:VehiculoService
  ) { }

  ngOnInit() {
    this.getVehiculos();
  }

  /**
   * getVehiculos solicita al servicio vehiculo.service.ts
   * todos los vehiculos registrados
   */
  getVehiculos(){
    this.vehiculoService.getVehiculos().subscribe(
      vehiculos=>{
        this.vehiculos=vehiculos;
        this.vehiculosFiltered=vehiculos;
      },
      err=>{
        console.log(err);
      }
    )
  }

  get listFilter(): string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter=value;
    this.vehiculosFiltered=this.listFilter ? this.performFilter(this.listFilter) : this.vehiculos;
    console.log(this.vehiculosFiltered);
  }

  performFilter(filterBy:string): Vehiculo[]{
    filterBy=filterBy.toLocaleLowerCase();
    return this.vehiculos.filter((vehiculo: Vehiculo)=>
      vehiculo.patente.toLocaleLowerCase().indexOf(filterBy) !== -1);   
  }

  
  public generatePDF() { 
    var data = document.getElementById('contentToConvert'); 
    html2canvas(data).then(canvas => { 
      // Few necessary setting options 
      var imgWidth = 208; 
      var pageHeight = 295; 
      var imgHeight = canvas.height * imgWidth / canvas.width; 
      var heightLeft = imgHeight; 
      
      const contentDataURL = canvas.toDataURL('image/png') 
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0; 
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight) 
      pdf.save('MYPdf.pdf'); // Generated PDF  
    }); 
  } 
}
